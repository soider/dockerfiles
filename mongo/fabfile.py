from __future__ import print_function
from fabric.api import task, local

from functools import wraps
from time import sleep


DOCKER_IP = '10.1.42.1'
MONGO_IMAGE_NAME = 'eithz/mongo'


def wait_after(seconds):
    def wrapper(func):
        @wraps(func)
        def inner_wrapper(*args, **kwargs):
            res = func(*args, **kwargs)
            sleep(seconds)
            return res
        return inner_wrapper
    return wrapper


def docker(cmd, cmd_args, **kwargs):
    def _stringify_arg(arg):
        if isinstance(arg, tuple):
            return ' '.join(arg)
        return arg

    args = map(_stringify_arg, cmd_args)
    cmd = 'docker %s %s' % (cmd, ' '.join(args))
    local(cmd, **kwargs)


def create_sky_dns(ip_addr):
    docker('run', ['-d',
                ('-p', '%s:53:53/udp' % ip_addr),
                ('--name', 'skydns'),
                'crosbymichael/skydns',
                ('-nameserver', '8.8.8.8:53'),
                ('-domain', 'docker')
        ])


def create_sky_dock():
    docker('run', ['-d',
                ('-v', '/var/run/docker.sock:/docker.sock'),
                ('--name', 'skydock'), 
                'crosbymichael/skydock',
                ('-ttl', '30'),
                ('-environment', 'dev'),
                ('-s', '/docker.sock'),
                ('-domain', 'docker'),
                ('-name', 'skydns')])


def local_db_storage_exists(localpath, node_num):
    return os.path.exists(os.path.join(localpath,
        'db',
        'node_%d' % node_num))


def get_db_path(localpath, node_num, shard_num):
    return os.path.join(localpath,
            'db',
            'node_%d' % node_num,
            'shard_%d' % shard_num)


def get_cfg_path(localpath, node_num):
    return os.path.join(localpath,
            'db',
            'node_%d' % node_num,
            'cfg')


def get_node_name(node_num, shard_num):
    return 'mongos%dr%d' % (node_num, shard_num) 


def create_local_db_storage(localpath, node_num, shards):
    for shard_num in shards:
        local('mkdir %s' % get_db_path(localpath, node_num, shard_num))
    local('mkdir %s' % get_cfg_path(localpath, node_num))


def create_mongo_server(node_num, shard_num, dns_addr, localpath):
    docker('run', 
        [('--dns', dns_addr),
         ('--name', get_node_name(node_num, shard_num)),
         '-P', 
         '-i', 
         '-d', 
         ('-v', '%s:/data/db' % get_db_path(localpath, node_num, shard_num)),
         '-e',
         "/usr/bin/mongod --replSet set${i} --dbpath /data/db --notablescan --noprealloc --smallfiles",
         MONGO_IMAGE_NAME])

@wait_after(30)
def setup_replica_set(node_num, dns_addr):
    pass


def eval_js_on(continer_name, js_code):
    ip, port = docker('port', [continer_name, '27017'], capture=True).split(':')[-1]
    local('mongo --port %d --eval %s' % (port, js_code))


def check_and_create_image(img_name):
    pass


@wait_after(20)
def create_mongo_node(node_num, shards, dns_addr, localpath):
    for shard_num in shards:
        create_mongo_server(node_num, shard_num, dns_addr)

@task
def create_mongo_rs(nodes=3, shards=2, localpath='./mongodbdata'):
    cneck_and_create_image('eithz/mongo')
    create_sky_dns(DOCKER_IP)
    create_sky_dock()
    for node_num in xrange(nodes):
        if not local_db_storage_exists(localpath, node_num):
            create_local_db_storage(localpath, node_num, shards)
        create_mongo_node(node_num, shards, DOCKER_IP, localpath) # done
        setup_replica_set(node_num, DOCKER_IP) # not done
        create_config_server(node_num, DOCKER_IP, localpath) # not done
    setup_mongo_router(shards)



    